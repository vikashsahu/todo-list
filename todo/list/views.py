import json

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render

from list.models import Task
from list.forms import TaskForm


@login_required
def index(request):
    user = request.user

    if request.method == 'POST':
        form = TaskForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.user = user

            instance.save()

    tasks = Task.objects.filter(user=user).filter(completed=False).order_by('end_date')

    return render(request, 'index.html', {
        'tasks': tasks,
        'form': TaskForm(),
    })


@login_required
def complete_task(request):
    if request.is_ajax():
        if request.method == 'POST':
            pk = int(request.POST['pk'])

            completed_task = Task.objects.get(pk=pk)
            completed_task.completed = True

            completed_task.save()

    return HttpResponse(json.dumps({'success': True}), content_type='application/json')
