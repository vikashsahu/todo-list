from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'list.views.index', name='index'),
    url(r'^complete$', 'list.views.complete_task', name='complete'),
)
